jQuery(document).ready(function ($) {
    "use strict";


    /**
     * =====================================
     * Feature Slider List Add/Remove Active Class   
     * =====================================
     */


    $(".feature-carousel-list li").on('click', function () {
        $(".feature-carousel-list li").removeClass("active");
        $(this).addClass("active");
    });

    
    /**
     * =====================================
     * Feature Double Slider   
     * =====================================
     */


    $('.feature-carousel-list li').on('click', function () {
        $('.carousel').carousel(parseInt(this.getAttribute('data-to')));
    });


    /**
     * =====================================
     * Accordion Header Active icon (+,-) Add or Remove      
     * =====================================
     */

    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('ion-minus ion-plus');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);



    /**
     * =====================================
     * Review or Testimonial Slider         
     * =====================================
     */

    $(".clients-reviews").owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds
        pagination: false,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]
    });


    /**
     * =====================================
     * Counter        
     * =====================================
     */

    $('.counter-single').counterUp({
        delay: 10,
        time: 1000
    });


    /**
     * =====================================
     * Function for email address validation         
     * =====================================
     */

    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };

    
    /**
     * ============================
     * CONTACT FORM 2
     * ============================
     */

    $("#contact-form-2").on('submit', function (e) {
        e.preventDefault();
        var success = $(this).find('.email-success'),
            failed = $(this).find('.email-failed'),
            loader = $(this).find('.email-loading'),
            postUrl = $(this).attr('action');

        var data = {
            name: $(this).find('.contact-name').val(),
            email: $(this).find('.contact-email').val(),
            subject: $(this).find('.contact-subject').val(),
            message: $(this).find('.contact-message').val()
        };

        if (isValidEmail(data['email']) && (data['message'].length > 1) && (data['name'].length > 1)) {
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                beforeSend: function () {
                    loader.fadeIn(1000);
                },
                success: function (data) {
                    loader.fadeOut(1000);
                    success.delay(500).fadeIn(1000);
                    failed.fadeOut(500);
                },
                error: function (xhr) { // if error occured
                    loader.fadeOut(1000);
                    failed.delay(500).fadeIn(1000);
                    success.fadeOut(500);
                },
                complete: function () {
                    loader.fadeOut(1000);
                }
            });
        } else {
            loader.fadeOut(1000);
            failed.delay(500).fadeIn(1000);
            success.fadeOut(500);
        }

        return false;
    });
    
/**
 * ============================
 * Sticky Header Activation
 * ============================
 */

$(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $('.header').addClass("sticky");
    } else {
        $('.header').removeClass("sticky");
    }
});


/**
 * ============================
 * One Page Menu
 * ============================
 */

        // Select all links with hashes
        $('a[href*="#"]')
          // Remove links that don't actually link to anything
          .not('[href="#"]')
          .not('[href="#0"]')
          .click(function(event) {
            // On-page links
            if (
              location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
              && 
              location.hostname == this.hostname
            ) {
              // Figure out element to scroll to
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000, function() {
                  // Callback after animation
                  // Must change focus!
                  var $target = $(target);
                  $target.focus();
                  if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                  } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                  };
                });
              }
            }
          });

        $(document).on('click','.navbar-collapse.in',function(e) {
        if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
            $(this).collapse('hide');
        }
		});
		
		$('ul.nav li.dropdown').hover(function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
    
    
});



